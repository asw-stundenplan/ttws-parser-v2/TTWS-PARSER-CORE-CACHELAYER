package de.notepass.ttws.core.cachelayer.autorenew.model;

import de.notepass.ttws.core.cachelayer.autorenew.CacheEntryReference;
import de.notepass.ttws.api.java.pojo.timetable.Timetable;
import org.ehcache.Cache;

import java.util.*;
import java.util.concurrent.*;

/**
 * Created by kim on 23.02.2018.
 */
public abstract class CacheRenewHandler {
    protected Map<CacheEntryReference, ScheduledFuture> entriesToKeepCurrentRunnables = new HashMap<CacheEntryReference, ScheduledFuture>();
    protected ScheduledExecutorService executorService;
    protected Cache<String, Timetable> cacheToKeepCurrent;
    protected final int renewTimeSeconds;
    protected final int numThreads;
    protected Random random = new Random();

    public CacheRenewHandler(Cache<String, Timetable> cacheToKeepCurrent, int renewTimeSeconds, int numThreads) {
        this.cacheToKeepCurrent = cacheToKeepCurrent;
        this.renewTimeSeconds = renewTimeSeconds;
        this.numThreads = numThreads;

        executorService = Executors.newScheduledThreadPool(numThreads);
    }

    public synchronized void addEntry(CacheEntryReference ref) {
        if (entriesToKeepCurrentRunnables.containsKey(ref)) {
            return;
        }

        Runnable r = generateRunnable(ref);
        ScheduledFuture future = executorService.scheduleAtFixedRate(r, 1+random.nextInt(renewTimeSeconds), renewTimeSeconds, TimeUnit.SECONDS);
        entriesToKeepCurrentRunnables.put(ref, future);
    }

    public synchronized void removeEntry(CacheEntryReference ref) {
        ScheduledFuture future = entriesToKeepCurrentRunnables.get(ref);
        if (future != null) {
            future.cancel(false);
            entriesToKeepCurrentRunnables.remove(ref);
        }
    }

    protected abstract Runnable generateRunnable(CacheEntryReference ref);
}
