package de.notepass.ttws.core.cachelayer;

import de.notepass.ttws.api.java.pojo.timetable.Timetable;
import de.notepass.ttws.api.java.pojo.timetable.TimetableWeek;
import de.notepass.ttws.core.timetable.TimeTableParser;
import de.notepass.ttws.core.cachelayer.autorenew.CacheEntryReference;
import de.notepass.ttws.core.cachelayer.autorenew.impl.TimetableCacheRenewHandler;
import org.ehcache.UserManagedCache;
import org.ehcache.config.builders.UserManagedCacheBuilder;

import java.security.InvalidParameterException;

/**
 * Created by kim on 23.02.2018.
 */
public class CachedTimetableParser {
    protected static UserManagedCache<String, Timetable> cache = null;
    protected static TimetableCacheRenewHandler renewHandler = null;


    public static synchronized void init() {
        if (cache == null) {
            cache = UserManagedCacheBuilder
                    .newUserManagedCacheBuilder(String.class, Timetable.class)
                    .identifier("timetablecache")
                    .build(true);

            renewHandler = new TimetableCacheRenewHandler(cache, 900, 4);
        }
    }

    public static Timetable getTimetableForBlock(String clazz, int block) throws Exception {
        init();
        CacheEntryReference ref = new CacheEntryReference(clazz, block);
        Timetable timetable = cache.get(ref.toCacheKey());

        if (timetable == null) {
            timetable = TimeTableParser.getTimetableForBlock(clazz, block);
            cache.put(ref.toCacheKey(), timetable);
            renewHandler.addEntry(ref);
        }

        return timetable;
    }

    public static Timetable getTimetableForBlockRange(String clazz, int startBlock, int endBlock) throws Exception {
        if (endBlock < startBlock) {
            throw new InvalidParameterException("endBlock should not be smaller than startBlock");
        }

        if (endBlock < 0) {
            throw new InvalidParameterException("endBlock should not be smaller than 1");
        }

        if (startBlock < 0) {
            throw new InvalidParameterException("startBlock should not be smaller than 1");
        }

        init();

        Timetable container = new Timetable();
        for (int i = startBlock; i <= endBlock; i++) {
            Timetable sub = getTimetableForBlock(clazz, i);
            for (TimetableWeek week : sub.getWeeks()) {
                container.getWeeks().add(week);
            }
        }

        return container;
    }
}
