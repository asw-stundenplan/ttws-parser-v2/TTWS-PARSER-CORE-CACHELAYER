package de.notepass.ttws.core.cachelayer.autorenew.impl;

import de.notepass.ttws.core.timetable.TimeTableParser;
import de.notepass.ttws.core.cachelayer.autorenew.CacheEntryReference;
import de.notepass.ttws.core.cachelayer.autorenew.model.CacheRenewHandler;
import org.ehcache.Cache;
import de.notepass.ttws.api.java.pojo.timetable.Timetable;

/**
 * Created by kim on 23.02.2018.
 */
public class TimetableCacheRenewHandler extends CacheRenewHandler {
    public TimetableCacheRenewHandler(Cache<String, Timetable> cacheToKeepCurrent, int renewTimeSeconds, int numThreads) {
        super(cacheToKeepCurrent, renewTimeSeconds, numThreads);
    }

    protected Runnable generateRunnable(final CacheEntryReference ref) {
        return new Runnable() {
            public void run() {
                try {
                    Timetable t = TimeTableParser.getTimetableForBlock(ref.getClazz(), ref.getBlock());
                    cacheToKeepCurrent.put(ref.toCacheKey(), t);
                } catch (Exception e) {
                    //TODO
                }
            }
        };
    }
}
