package de.notepass.ttws.core.cachelayer.autorenew;

/**
 * Created by kim on 23.02.2018.
 */
public class CacheEntryReference {
    private static String cacheKeyTemplate = "%s%d";
    private String clazz;
    private int block;

    public CacheEntryReference(String clazz, int block) {
        this.clazz = clazz;
        this.block = block;
    }

    public String getClazz() {
        return clazz;
    }

    public int getBlock() {
        return block;
    }

    public String toCacheKey() {
        return String.format(cacheKeyTemplate, clazz, block);
    }
}
