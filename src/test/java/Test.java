import de.notepass.ttws.api.java.pojo.timetable.Timetable;
import de.notepass.ttws.core.cachelayer.CachedTimetableParser;

/**
 * Created by kim on 23.02.2018.
 */
public class Test {
    public static void main(String[] args) throws Exception {
        Timetable t;
        long sTime = 0;

        System.out.println("BOOTSTRAPPING EVERYTHING");
        try {
            CachedTimetableParser.getTimetableForBlock("WII", 9);
        } catch (Exception e) {
            //Ignore
        }
        Thread.sleep(2000);
        System.out.println("Done");

        System.out.println("========================================");
        System.out.println("Normal request");
        sTime = System.currentTimeMillis();
        t = CachedTimetableParser.getTimetableForBlockRange("WIA18", 6, 9);
        System.out.println("Done; "+(System.currentTimeMillis()-sTime)+"ms");
        System.out.println("========================================");
        System.out.println();
        System.out.println("========================================");
        System.out.println("Cached request");
        sTime = System.currentTimeMillis();
        t = CachedTimetableParser.getTimetableForBlockRange("WIA18", 6, 9);
        System.out.println("Done; "+(System.currentTimeMillis()-sTime)+"ms");
        System.out.println("========================================");
    }
}
